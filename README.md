# login-json
The commands below are used to preview the vue components however the components are planned to 
be exported and used seperately by a main vue project easily. Instructions will be included later.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

